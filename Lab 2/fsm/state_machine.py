##############
#### Your name: Ashvin Warrier and James Lowe
##############

import cozmo
from cozmo.util import degrees, distance_mm, speed_mmps
import numpy as np
from imgclassification import ImageClassifier
from enum import Enum
import time

robot: cozmo.robot.Robot


class State(Enum):
    IDLE = 0
    BURN = 1
    SOS = 2
    DEFUSE = 3
    QUIT = 4


def detect(ic):
    while not robot.world.latest_image:
        pass
    img = robot.world.latest_image.raw_image
    features = ic.extract_image_features([np.array(img)])
    [label] = ic.predict_labels(features)
    return label


def idle(ic):
    robot.set_head_angle(degrees(0)).wait_for_completed()
    label = detect(ic)
    if label == 'inspection':
        robot.say_text('inspection').wait_for_completed()
        return State.BURN
    if label == 'drone':
        robot.say_text('drone').wait_for_completed()
        return State.SOS
    if label == 'order':
        robot.say_text('order').wait_for_completed()
        return State.DEFUSE

    return State.IDLE


def drive_in_square():
    robot.drive_wheels(50, 50)
    robot.set_lift_height(1, duration=2).wait_for_completed()
    robot.set_lift_height(0, duration=2).wait_for_completed()
    robot.stop_all_motors()
    robot.turn_in_place(degrees(90)).wait_for_completed()


def burn():
    # Drive in a square while raising and lowering forklift
    # return to Idle
    robot.say_text('I am not a spy').wait_for_completed()
    for i in range(4):
        drive_in_square()

    robot.set_lift_height(0, duration=1).wait_for_completed()

    return State.IDLE


def sos():
    print('SOSing')
    # Have the robot drive in an “S” formation. Show an animation on the robot’s face
    # return to the Idle state.
    robot.turn_in_place(degrees(-90)).wait_for_completed()
    robot.play_anim_trigger(cozmo.anim.Triggers.CodeLabExcited)
    robot.drive_wheel_motors(30, 40)
    time.sleep(2)
    robot.drive_wheel_motors(20, 60)
    time.sleep(5)
    robot.play_anim_trigger(cozmo.anim.Triggers.CodeLabChatty)
    robot.drive_wheel_motors(60, 20)
    time.sleep(4)
    robot.drive_wheel_motors(30, 10)
    time.sleep(4)
    robot.stop_all_motors()
    return State.IDLE


def defuse():
    # Place a cube at point C on the arena. Start your robot at point D on the arena and directly face the cube.
    # The robot should locate the cube (any cube if you have more than one), pick up the cube, drive forward with
    # the cube to the end of the arena (point A), put down the cube, and drive backward to the robot’s starting location.
    # return to the Idle state.
    robot.set_head_angle(degrees(0)).wait_for_completed()
    cube = robot.world.get_light_cube(cozmo.objects.LightCube1Id)
    robot.pickup_object(cube, num_retries=3).wait_for_completed()
    robot.drive_straight(distance_mm(35 * 10), speed_mmps(50)).wait_for_completed()
    robot.place_object_on_ground_here(cube, num_retries=2).wait_for_completed()
    robot.drive_straight(distance_mm(-35 * 10), speed_mmps(50)).wait_for_completed()
    return State.IDLE


def mainloop():
    state = State.IDLE
    ic = ImageClassifier()
    (train_raw, train_labels) = ic.load_data_from_folder('../train/')
    train_data = ic.extract_image_features(train_raw)
    ic.train_classifier(train_data, train_labels)

    while True:
        if state == State.IDLE:
            next_state = idle(ic)
        elif state == State.BURN:
            next_state = burn()
        elif state == State.SOS:
            next_state = sos()
        elif state == State.DEFUSE:
            next_state = defuse()
        else:
            print('quit')
            break
        state = next_state


def cozmo_program(_robot):
    global robot
    robot = _robot
    robot.camera.color_image_enabled = False
    robot.camera.image_stream_enabled = True
    robot.camera.enable_auto_exposure()
    mainloop()


if __name__ == '__main__':
    cozmo.run_program(cozmo_program)
