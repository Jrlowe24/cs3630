def find_common_denominator(num1, num2):
    if num2 == 0:
        return num1
    next = num1 % num2
    return find_common_denominator(num2, next)


def solution(X, Y):
    # write your code in Python 3.
    hashMap = {}

    for i in range(len(X)):
        numerator = X[i] // find_common_denominator(X[i], Y[i])
        print(numerator)
        denominator = Y[i] // find_common_denominator(X[i], Y[i])
        print(denominator)
        hashMap[(numerator, denominator)] += 1

    return len(hashMap)


if __name__ == "__main__":

    # Numerator Array
    num = [1, 40, 20, 5, 6, 7]

    # Denominator Array
    den = [10, 40, 2, 5, 12, 14]

    print(solution(num, den))