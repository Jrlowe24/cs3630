#!/usr/bin/env python

##############
#### Your name: Ashvin Warrier and James Lowe
##############

import numpy as np
import re
from sklearn import svm, metrics
from skimage import io, feature, filters, exposure, color, measure


class ImageClassifier:
    
    def __init__(self):
        self.classifier = None

    def imread_convert(self, f):
        return io.imread(f).astype(np.uint8)

    def load_data_from_folder(self, dir):
        # read all images into an image collection
        ic = io.ImageCollection(dir+"*.bmp", load_func=self.imread_convert)
        
        # create one large array of image data
        data = io.concatenate_images(ic)
        
        # extract labels from image names
        labels = np.array(ic.files)
        for i, f in enumerate(labels):
            m = re.search("_", f)
            labels[i] = f[len(dir):m.start()]
        
        return data, labels

    def extract_image_features(self, data, grayscale=False):
        # Please do not modify the header above

        # extract feature vector from image data
        feature_data = []
        for img in data:
            #  convert to GrayScale
            if grayscale:
                gray = img
            else:
                gray = color.rgb2gray(img)

            #  equalizes image pixels based on lighting conditions
            gray = exposure.equalize_hist(gray)
            #  Apply gaussian filter for smoothing
            gray = filters.gaussian(gray)

            features = feature.hog(gray, orientations=12, pixels_per_cell=(40, 40), cells_per_block=(4, 4),
                                   block_norm='L2-Hys')

            feature_data.append(features)

        # Please do not modify the return type below
        return feature_data

    def train_classifier(self, train_data, train_labels):
        # Please do not modify the header above
        
        # train model and save the trained model to self.classifier

        self.classifier = svm.SVC().fit(train_data, train_labels)

    def predict_labels(self, data):
        # Please do not modify the header

        # predict labels of test data using trained model in self.classifier
        # the code below expects output to be stored in predicted_labels
        
        ########################
        predicted_labels = self.classifier.predict(data)
        ########################

        # Please do not modify the return type below
        return predicted_labels

    def line_fitting(self, data):
        # Please do not modify the header

        # fit a line the to arena wall using RANSAC
        # return two lists containing slopes and y intercepts of the line
        slope = []
        intercept = []
        ########################
        for img in data:
            #  convert to GrayScale
            gray = color.rgb2gray(img)
            # gray = exposure.equalize_hist(gray)
            gray = filters.gaussian(gray, 4)
            edges = feature.canny(gray, sigma=2)

            line,_ = measure.ransac(np.argwhere(edges == 1), measure.LineModelND, 2, 0.5, max_trials=500)
            origin, direction = line.params
            intercept.append((origin[0] - (direction[0] / direction[1]) * origin[1]))
            slope.append(direction[0] / direction[1])
            edge_locs = np.where(edges != [0])
            x = edge_locs[1]
            y = edge_locs[0]
            # x_inliers = []
            # y_inliers = []
            # best_line = None
            # best_indices = []
            # max_inliers = 0
            # s = 2  # samples
            # for j in range(1000):
            #     # print(past_inliers)
            #     x_inliers = []
            #     y_inliers = []
            #     indices = []
            #     while len(indices) < s:
            #         # if len(x_inliers) > 1:
            #         #     rand_ind = random.randint(0, len(x_inliers) - 1)
            #         # else:
            #         rand_ind = random.randint(0, len(x) - 1)
            #         if rand_ind not in indices:
            #             indices.append(rand_ind)
            #     # if len(x_inliers) > 1:
            #     #     line = np.polyfit([x_inliers[i] for i in indices], [y_inliers[i] for i in indices], 1)
            #     # else:
            #     line = np.polyfit([x[i] for i in indices], [y[i] for i in indices], 1)
            #
            #     threshold = 1  # threshold for inline points
            #     # if len(x_inliers) > 1:
            #     #     for p in range(0, len(x_inliers)):
            #     #         y_pred = line[0] * x_inliers[p] + line[1]
            #     #         if y_pred - y_inliers[p] < threshold:
            #     #             inlier_count += 1
            #     #             x_inliers.append(x_inliers[p])
            #     #             y_inliers.append(y_inliers[p])
            #     # else:
            #     for p in range(0, len(x)):
            #         y_pred = line[0] * x[p] + line[1]
            #         if abs(y_pred - y[p]) < threshold:
            #
            #             x_inliers.append(x[p])
            #             y_inliers.append(y[p])
            #
            #     if len(x_inliers) > max_inliers:
            #         best_line = line
            #         best_indices = indices[0], indices[1]
            #
            # slope.append(best_line[0])
            # intercept.append(best_line[1])
            #
            # fig, (ax1, ax2, ax3) = plt.subplots(nrows=1, ncols=3, figsize=(8, 3),
            #                                     sharex=True, sharey=True)
            #
            # ax1.imshow(edges, cmap=plt.cm.gray)
            # ax1.axis('off')
            # ax1.set_title('noisy image', fontsize=20)
            #
            # ax2.imshow(gray, cmap=plt.cm.gray)
            #
            # plot_x = list(range(1,300))
            # plot_y = [x * direction[0] / direction[1] + (origin[0] - (direction[0] / direction[1]) * origin[1]) for x in plot_x]
            # ax2.plot(plot_x, plot_y, 'r--')
            # # print(best_indices)
            # # ax2.plot(x[best_indices[0]], y[best_indices[0]], 'g.')
            # # ax2.plot(x[best_indices[1]], y[best_indices[1]], 'g.')
            # plt.show()


            # how many
        ########################

        # Please do not modify the return type below
        return slope, intercept
def main():

    img_clf = ImageClassifier()

    # load images
    (train_raw, train_labels) = img_clf.load_data_from_folder('/train/')
    (test_raw, test_labels) = img_clf.load_data_from_folder('./test/')
    (wall_raw, _) = img_clf.load_data_from_folder('./wall/')
    
    # convert images into features
    train_data = img_clf.extract_image_features(train_raw)
    test_data = img_clf.extract_image_features(test_raw)
    
    # train model and test on training data
    img_clf.train_classifier(train_data, train_labels)
    predicted_labels = img_clf.predict_labels(train_data)
    print("\nTraining results")
    print("=============================")
    print("Confusion Matrix:\n",metrics.confusion_matrix(train_labels, predicted_labels))
    print("Accuracy: ", metrics.accuracy_score(train_labels, predicted_labels))
    print("F1 score: ", metrics.f1_score(train_labels, predicted_labels, average='micro'))
    
    # test model
    predicted_labels = img_clf.predict_labels(test_data)
    print("\nTest results")
    print("=============================")
    print("Confusion Matrix:\n",metrics.confusion_matrix(test_labels, predicted_labels))
    print("Accuracy: ", metrics.accuracy_score(test_labels, predicted_labels))
    print("F1 score: ", metrics.f1_score(test_labels, predicted_labels, average='micro'))

    # ransac
    print("\nRANSAC results")
    print("=============================")
    s, i = img_clf.line_fitting(wall_raw)
    print(f"Line Fitting Score: {ransac_score.score(s,i)}/10")

if __name__ == "__main__":
    main()
