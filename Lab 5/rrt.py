import cozmo
import math
import sys
import time

from cmap import *
from gui import *
from utils import *
import numpy as np
import random
from random import randint
import copy
from cozmo.util import distance_mm, speed_mmps

MAX_NODES = 20000


def step_from_to(node0, node1, limit=75):
    ########################################################################
    # TODO: please enter your code below.
    # 1. If distance between two nodes is less than limit, return node1
    # 2. Otherwise, return a node in the direction from node0 to node1 whose
    #    distance to node0 is limit. Recall that each iteration we can move
    #    limit units at most
    # 3. Hint: please consider using np.arctan2 function to get vector angle
    # 4. Note: remember always return a Node object

    if get_dist(node0, node1) < limit:
        return node1
    else:
        dy = node1.y - node0.y
        dx = node1.x - node0.x
        angle = np.arctan2(dx, dy)
        x = node0.x + (limit * np.cos(angle))
        y = node0.y + (limit * np.sin(angle))
        return Node((x,y))
    ############################################################################
    
    
def node_generator(cmap):
    rand_node = None
    ############################################################################
    # TODO: please enter your code below.
    # 1. Use CozMap width and height to get a uniformly distributed random node
    # 2. Use CozMap.is_inbound and CozMap.is_inside_obstacles to determine the
    #    legitimacy of the random node.
    # 3. Note: remember always return a Node object

    if randint(1,100) <= 5:  # 5% chance of returning the goal node
        goal = copy.deepcopy(cmap.get_goals()[0])
        # goal = cmap.get_goals()[0]
        return goal
    else:
        map_size = cmap.get_size()
        rand_node = Node((map_size[0] * random.random(), map_size[1] * random.random()))
        while not cmap.is_inbound(rand_node) or cmap.is_inside_obstacles(rand_node):
            rand_node = Node((map_size[0] * random.random(), map_size[1] * random.random()))
        return rand_node
    ############################################################################

def closest_node(cmap, rand_node):
    ########################################################################
    # Finds the closest node to rand_node and returns
    nearest = None
    min_dist = float('inf')
    for node in cmap.get_nodes():
        dist = get_dist(rand_node, node)
        if dist < min_dist:
            min_dist = dist
            nearest = node

    return nearest
    ########################################################################


def RRT(cmap, start):
    cmap.add_node(start)
    map_width, map_height = cmap.get_size()
    while (cmap.get_num_nodes() < MAX_NODES):
        ########################################################################
        # TODO: please enter your code below.
        # 1. Use CozMap.get_random_valid_node() to get a random node. This
        #    function will internally call the node_generator above
        # 2. Get the nearest node to the random node from RRT
        # 3. Limit the distance RRT can move
        # 4. Add one path from nearest node to random node
        #
        
        #temporary code below to be replaced
        rand_node = cmap.get_random_valid_node()
        nearest_node = closest_node(cmap, rand_node)
        step_from_to(rand_node, nearest_node)
        ########################################################################
        time.sleep(0.01)
        cmap.add_path(nearest_node, rand_node)
        if cmap.is_solved():
            break

    path = cmap.get_path()
    smoothed_path = cmap.get_smooth_path()

    print(cmap.is_solution_valid())

    if cmap.is_solution_valid():
        print("A valid solution has been found :-) ")
        print("Nodes created: ", cmap.get_num_nodes())
        print("Path length: ", len(path))
        print("Smoothed path length: ", len(smoothed_path))
    else:
        print("Please try again :-(")

def get_pose(robot):
    start_x = 50
    start_y = 35
    current_x = robot.pose.position.x
    current_y = robot.pose.position.y
    return Node((current_x + start_x, current_y + start_y))


async def CozmoPlanning(robot: cozmo.robot.Robot):
    # Allows access to map and stopevent, which can be used to see if the GUI
    # has been closed by checking stopevent.is_set()
    global cmap, stopevent

    ########################################################################
    # TODO: please enter your code below.
    # Description of function provided in instructions. Potential pseudcode is below

    #assume start position is in cmap and was loaded from emptygrid.json as [50, 35] already
    #assume start angle is 0
    cozmo_pos = Node((50, 35))
    cozmo_angle = 0.0

    #Add final position as goal point to cmap, with final position being defined as a point that is at the center of the arena 
    #you can get map width and map weight from cmap.get_size()
    map_width, map_height = cmap.get_size()
    goal_node = Node((int(map_width / 2), int(map_height / 2)))
    cmap.add_goal(goal_node)

    #reset the current stored paths in cmap
    cmap.reset_paths()
    #call the RRT function using your cmap as input, and RRT will update cmap with a new path to the target from the start position
    #get path from the cmap
    cmap.set_start(cozmo_pos)
    RRT(cmap, cmap.get_start())
    await robot.set_head_angle(cozmo.util.degrees(0)).wait_for_completed()
    
    #marked and update_cmap are both outputted from detect_cube_and_update_cmap(robot, marked, cozmo_pos).
    #and marked is an input to the function, indicating which cubes are already marked
    #So initialize "marked" to be an empty dictionary and "update_cmap" = False
    marked_cubes = {}
    next_path_position = 1
    path = cmap.get_smooth_path()
    path_computed = True
    next_motion = False
    curr_node = None
    #while the current cosmo pos
    while next_path_position < len(path):

        if not path_computed:

            cmap.reset_paths()
            cmap.set_start(curr_node)
            RRT(cmap, cmap.get_start())
            path = cmap.get_smooth_path()
            next_path_position = 1
            path_computed = True

        #break if path is none or empty, indicating no path was found
        if cmap.get_path() is None or len(cmap.get_path()) is 0:
            print('No path found')
            break
        
        # Get the next node from the path
        curr_node = path[next_path_position - 1]
        next_node = path[next_path_position]

        #drive the robot to next node in path. #First turn to the appropriate angle, and then move to it
        #you can calculate the angle to turn through a trigonometric function
        dx = next_node.x - curr_node.x
        dy = next_node.y - curr_node.y
        angle = np.arctan2(dx, dy)
        
        updated = False
        await robot.turn_in_place(cozmo.util.Angle(radians = math.pi/2 - angle), is_absolute = True).wait_for_completed()

        update_cmap, goal, marked_cubes = await detect_cube_and_update_cmap(robot, marked_cubes, get_pose(robot))
        if update_cmap:
            updated = True
        
        if updated:
            next_motion = True
            path_computed = False
        else:
            next_motion = False

        if not next_motion:
            distance = get_dist(curr_node, next_node)
            print(distance)
            await robot.drive_straight(distance_mm(distance), speed_mmps(50)).wait_for_completed()
            cmap.set_start(next_node)
            next_motion = True
            next_path_position += 1
            
        
        # Update the current Cozmo position (cozmo_pos and cozmo_angle) to be new node position and angle 
    
    ########################################################################
    
    
    
    
def get_global_node(local_angle, local_origin, node):
    """Helper function: Transform the node's position (x,y) from local coordinate frame specified by local_origin and local_angle to global coordinate frame.
                        This function is used in detect_cube_and_update_cmap()
        Arguments:
        local_angle, local_origin -- specify local coordinate frame's origin in global coordinate frame
        local_angle -- a single angle value
        local_origin -- a Node object
        Outputs:
        new_node -- a Node object that decribes the node's position in global coordinate frame
    """
    ########################################################################
    # TODO: please enter your code below.
    
    #temporary code below to be replaced
    node_x, node_y = node.x, node.y
    loc_x, loc_y = local_origin.x, local_origin.y

    x = (node_x * np.cos(local_angle)) + (-node_y * np.sin(local_angle)) + loc_x
    y = (node_x * np.sin(local_angle)) + (node_y * np.cos(local_angle)) + loc_y

    return Node((x, y))
    ########################################################################


async def detect_cube_and_update_cmap(robot, marked, cozmo_pos):
    """Helper function used to detect obstacle cubes and the goal cube.
       1. When a valid goal cube is detected, old goals in cmap will be cleared and a new goal corresponding to the approach position of the cube will be added.
       2. Approach position is used because we don't want the robot to drive to the center position of the goal cube.
       3. The center position of the goal cube will be returned as goal_center.

        Arguments:
        robot -- provides the robot's pose in G_Robot
                 robot.pose is the robot's pose in the global coordinate frame that the robot initialized (G_Robot)
                 also provides light cubes
        cozmo_pose -- provides the robot's pose in G_Arena
                 cozmo_pose is the robot's pose in the global coordinate we created (G_Arena)
        marked -- a dictionary of detected and tracked cubes (goal cube not valid will not be added to this list)

        Outputs:
        update_cmap -- when a new obstacle or a new valid goal is detected, update_cmap will set to True
        goal_center -- when a new valid goal is added, the center of the goal cube will be returned
    """
    global cmap

    # Padding of objects and the robot for C-Space
    cube_padding = 40.
    cozmo_padding = 100.

    # Flags
    update_cmap = False
    goal_center = None

    # Time for the robot to detect visible cubes
    time.sleep(1)

    for obj in robot.world.visible_objects:

        if obj.object_id in marked:
            continue
        print('looping thru visibly objects', obj)

        # Calculate the object pose in G_Arena
        # obj.pose is the object's pose in G_Robot
        # We need the object's pose in G_Arena (object_pos, object_angle)
        dx = obj.pose.position.x - robot.pose.position.x
        dy = obj.pose.position.y - robot.pose.position.y

        object_pos = Node((cozmo_pos.x+dx, cozmo_pos.y+dy))
        object_angle = obj.pose.rotation.angle_z.radians

        # Define an obstacle by its four corners in clockwise order
        obstacle_nodes = []
        obstacle_nodes.append(get_global_node(object_angle, object_pos, Node((cube_padding, cube_padding))))
        obstacle_nodes.append(get_global_node(object_angle, object_pos, Node((cube_padding, -cube_padding))))
        obstacle_nodes.append(get_global_node(object_angle, object_pos, Node((-cube_padding, -cube_padding))))
        obstacle_nodes.append(get_global_node(object_angle, object_pos, Node((-cube_padding, cube_padding))))
        cmap.add_obstacle(obstacle_nodes)
        marked[obj.object_id] = obj
        update_cmap = True

    return update_cmap, goal_center, marked


class RobotThread(threading.Thread):
    """Thread to run cozmo code separate from main thread
    """

    def __init__(self):
        threading.Thread.__init__(self, daemon=True)

    def run(self):
        # Please refrain from enabling use_viewer since it uses tk, which must be in main thread
        cozmo.run_program(CozmoPlanning,use_3d_viewer=False, use_viewer=False)
        stopevent.set()


class RRTThread(threading.Thread):
    """Thread to run RRT separate from main thread
    """

    def __init__(self):
        threading.Thread.__init__(self, daemon=True)

    def run(self):
        while not stopevent.is_set():
            RRT(cmap, cmap.get_start())
            time.sleep(100)
            cmap.reset_paths()
        stopevent.set()


if __name__ == '__main__':
    global cmap, stopevent
    stopevent = threading.Event()
    robotFlag = False
    for i in range(0,len(sys.argv)): #reads input whether we are running the robot version or not
        if (sys.argv[i] == "-robot"):
            robotFlag = True
    if (robotFlag):
        #creates cmap based on empty grid json
        #"start": [50, 35],
        #"goals": [] This is empty
        cmap = CozMap("maps/emptygrid.json", node_generator) 
        robot_thread = RobotThread()
        robot_thread.start()
    else:
        cmap = CozMap("maps/map2.json", node_generator)
        sim = RRTThread()
        sim.start()
    visualizer = Visualizer(cmap)
    visualizer.start()
    stopevent.set()
