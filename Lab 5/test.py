def f(n):
    for i in [x for x in range(n) if x % 2 == 1]:
        yield i
    for i in [x for x in range(n) if x % 2 == 0]:
        yield i
    

x = [n for n in f(5)]


class A:
    def foo(self):
        print('A foo')

class B(A):
    def foo(self):
        print('B foo')
    def bar(self):
        return self.foo()

class C(A):
    def foo(self):
        print('C foo')
    def bar(self):
        return self.foo()
class D(B,C):
    def foo(self):
        print('D foo')

D().bar()