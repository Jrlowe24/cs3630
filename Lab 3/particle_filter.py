from grid import CozGrid
from particle import Particle
from utils import grid_distance, rotate_point, diff_heading_deg, add_odometry_noise
from setting import *
import math
import numpy as np


def motion_update(particles, odom, grid):
    """ Particle filter motion update

        Arguments:
        particles -- input list of particle represents belief p(x_{t-1} | u_{t-1})
                before motion update
        odom -- odometry to move (dx, dy, dh) in *robot local frame*
        grid -- grid world map, which contains the marker information,
                see grid.py and CozGrid for definition
                Can be used for boundary checking
        Returns: the list of particles represents belief \tilde{p}(x_{t} | u_{t})
                after motion update
    """
    motion_particles = []  # create empty array to store new particles
    for particle in particles:  # loop through all the particles
        x_1, y_1, h_1 = particle.xyh  # extract old x, y, and h from the particle
        noise = add_odometry_noise(odom, 2, 0.02)  # add noise to the movement offset
        dh = noise[2]
        dx, dy = rotate_point(noise[0], noise[1], particle.h)  # gets local dx and dy by rotating 2d frame
        x_2 = x_1 + dx  # new x position
        y_2 = y_1 + dy  # new y position
        h_2 = h_1 + dh  # new heading
        motion_particles.append(Particle(x_2, y_2, h_2))  # create and add new particle to list

    return motion_particles


# ------------------------------------------------------------------------
def measurement_update(particles, measured_marker_list, grid):
    """ Particle filter measurement update

        Arguments:
        particles -- input list of particle represents belief \tilde{p}(x_{t} | u_{t})
                before measurement update (but after motion update)

        measured_marker_list -- robot detected marker list, each marker has format:
                measured_marker_list[i] = (rx, ry, rh)
                rx -- marker's relative X coordinate in robot's frame
                ry -- marker's relative Y coordinate in robot's frame
                rh -- marker's relative heading in robot's frame, in degree

                * Note that the robot can only see markers which is in its camera field of view,
                which is defined by ROBOT_CAMERA_FOV_DEG in setting.py
				* Note that the robot can see mutliple markers at once, and may not see any one

        grid -- grid world map, which contains the marker information,
                see grid.py and CozGrid for definition
                Can be used to evaluate particles

        Returns: the list of particles represents belief p(x_{t} | u_{t})
                after measurement update
    """
    particle_weights = []
    if len(measured_marker_list) != 0:
        for particle in particles:
            measured_marker_list_copy = measured_marker_list[:]
            particle_marker_list = particle.read_markers(grid)
            prob = 1.0
            if len(particle_marker_list) is 0:
                prob = SPURIOUS_DETECTION_RATE ** 2
            else:
                for marker in particle_marker_list:
                    closestDistance = None
                    chosen = None
                    for robotMarker in measured_marker_list_copy:
                        if len(measured_marker_list_copy) is 0 or len(particle_marker_list) is 0:
                            break

                        d = grid_distance(marker[0], marker[1], robotMarker[0], robotMarker[1])
                        measured_marker_list_copy.remove(robotMarker)
                        if closestDistance is None or d < closestDistance:
                            chosen = robotMarker
                            closestDistance = d

                    particle_marker_list.remove(marker)

                    if closestDistance is None: 
                        break

                    prob *= np.exp(-1 * (((closestDistance ** 2) / (2 * (MARKER_TRANS_SIGMA ** 2))) + (
                                ((diff_heading_deg(chosen[2], marker[2])) ** 2) / (2 * (MARKER_ROT_SIGMA ** 2)))))

            if not grid.is_in(particle.xy[0], particle.xy[1]):
                prob = 0

            particle_weights.append(prob)

        normalized = []
        summation = 0

        for weight in particle_weights:
            summation += weight

        for weight in particle_weights:
            if summation > 0:
                normalized.append(weight / summation)
            else:
                normalized.append(1 / len(particle_weights))

        sampled = np.random.choice(particles, len(particles), p=normalized)

        newParts = Particle.create_random(PARTICLE_COUNT - len(sampled), grid)
        for p in newParts:
            sampled.append(p)
        return sampled
    else:
        return np.random.choice(particles, len(particles))


